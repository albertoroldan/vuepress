module.exports = {
    title: 'GitLab ❤️ VuePress',
    description: 'Vue-powered static site generator running on GitLab Pages',
    base: '/vuepress/',
    dest: 'public'

    themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Guide', link: '/guide/' },
      { text: 'External', link: 'https://google.com' },
    ]
    sidebar: [
      '/',
      '/page-a',
      ['/page-b', 'Explicit link text']
    ]
  }
}